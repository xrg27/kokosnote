<?php
#   this is where you set your password, replace 'Example' with your desired password, 
#   $password = hash('sha256', 'MyDesiredPassword');
#   alternatively, you can replace the hash function with a pre-hashed password. For example:
#   $password = 'd029f87e3d80f8fd9b1be67c7426b4cc1ff47b4a9d0a8461c826a59d8c5eb6cd'
    $password = hash('sha256', 'Example');
    
#   how do you want your notes sorted?
#   options: 'alphabetical', 'last edited'
    $note_sort_order = 'alphabetical';
    
#   how many days should your login be remembered?
    $session_length = 1;    
?>
