# 🥥 kokosnote 

`kokosnote` is a _simple, under-engineerd_ note taking app. It can be deployed anywhere that runs PHP.

`kokosnote` is for you if:
- you want to securely share notes
- you have a webserver somewhere with PHP
- you dislike hassle

----
screenshot go here

----

### 🥥 features:
- barebones
- notes are stored in plain text on your server.
- uses [easy markdown editor (github.com)](https://github.com/Ionaru/easy-markdown-editor).
- besides this, no other dependencies.
- password protection
- drag-and-drop ftp deploy

----

### 🥥 installation:

1. download this repository as a .zip
2. extract the contents to your server, preferrably in a subdomain (kokos.example.com, for example)
3. (now you should have kokos.example.com/index.php, etc.)
4. change your password in login.php
5. done

p.s.:\
for Linux users: to test the application locally, simply `cd` to the root folder and run `php -S localhost:8000`\
then navigate to `localhost` in your favorite browser. The default password is `Example`

----

### 🥥 advanced:

Your notes are stored in /data/. Each category gets it's own folder, each not it's own .md file.\
so, your note about kimchi in recipies is found in /data/recipies/kimchi.md.

If you ever feel the need to back up your notes locally, just get the /data/ folder via ftp.\
alternatively, if you're that type, you can call `kokos.example.com/functions/takeout.php?supersecretpassword=pwd` to download all your notes as a zip.

/data/ is theorhetically served to the public, however, an .htaccess should prevent this. Please try this out by visiting `kokos.example.com/data/` to see if you get a `403 forbidden`.

----
### 🥥 why?:

I tried various self-hosted note taking solutions, but all of them were over-engineerd, bloated, or a bit too simple.\
Note taking should not require a _whole operating system_.\
So, here you go. Let me know if there's anything wrong with it.
