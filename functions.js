

var v_category = null;
var v_categories = null;
var v_notes = null;
var v_note_path = null;
var v_note_filetime = null;


window.onbeforeunload = function(e) {
    if (!check_for_edits())
    {
        return 'There are unsaved changes for note\n' + v_note_path;
    }
};


function refresh_categories(){
 var oReq = new XMLHttpRequest();
    oReq.onload = function() {
        var categories = JSON.parse(this.responseText);
        v_categories = categories;
        categories_list = document.getElementById("n_categories")
        while (categories_list .firstChild) {categories_list .removeChild(categories_list .firstChild);}
        for (let c in categories) {
            categories[c] = categories[c].split('/').pop();
            cat = categories[c];
            var node = document.createElement("LI");
            var textnode = document.createTextNode(cat);
            node.appendChild(textnode); 
            categories_list.appendChild(node); 
            node.setAttribute("onclick",`select_category("${cat}");`);
            node.setAttribute("title", cat);
        }
        update_selected_highlight();
    };
    oReq.open("get", "functions/list_categories.php" + "?trick=" + Date.now(), true);
    oReq.send();
}



function refresh_notes(){
 var oReq = new XMLHttpRequest();
    oReq.onload = function() {
        var notes = JSON.parse(this.responseText);
        v_notes = notes;
        notes_list = document.getElementById("n_notes")
        while (notes_list.firstChild) {notes_list.removeChild(notes_list.firstChild);}
        for (let c in notes ) {
            notes[c] = notes[c].split('/').pop();
            notes[c] = notes[c].split('.')[0];
            note = notes[c];
            
            var node = document.createElement("LI");
            var textnode = document.createTextNode(note);
            node.appendChild(textnode); 
            notes_list.appendChild(node); 
            node.setAttribute("onclick",`select_note("${note}");`);
            node.setAttribute("title", note);
        }
        update_selected_highlight();
    };
    oReq.open("get", "functions/list_notes.php?cat=" + encodeURIComponent(v_category) + "&trick=" + Date.now() , true);
    oReq.send();
}



function save_note(){
    if(v_note_path === null)
    {
        return;
    }
    var xhr = new XMLHttpRequest();
    xhr.open('PUT', 'functions/save_note.php?note=' + encodeURIComponent(v_note_path)  + "&filetime=" + encodeURIComponent(v_note_filetime) + "&trick=" + Date.now());
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onload = function() {
        if (xhr.status === 200) {
            var resp = JSON.parse(this.responseText);

            if (resp.hasOwnProperty('overwritten'))
            {
                popToast(resp.overwritten, false);
                refresh_notes();
            }
            else
            {        
                popToast("Note saved.", true);
                refresh_notes();
            }
            previous_content = easyMDE.value();
            v_note_filetime = resp.filetime;
        }
    };
    xhr.send(easyMDE.value());
}



function select_category(cat){
    v_category = cat;
    refresh_notes();
    update_selected_highlight();
}




function load_note(){
 var oReq = new XMLHttpRequest();
    oReq.onload = function() {
        if (oReq.status === 404)
            return;
        var note = JSON.parse(this.responseText);
        previous_content = note.note;
        v_note_filetime = note.filetime;
        easyMDE.value(previous_content);
        previous_content = easyMDE.value();
        document.getElementById('n_body').classList.remove('n_inactive');
        update_breadcrumb();
        if(!easyMDE.isPreviewActive())
        {
            easyMDE.togglePreview();
        }
        update_selected_highlight();
        set_last_loaded_cookie();
        document.getElementById('n_sidebar').classList.add("foldaway");

		list_attachments();
        
    };
    oReq.open("get", "functions/load_note.php?note=" + encodeURIComponent(v_note_path) + "&trick=" + Date.now(), true);
    oReq.send();
}

function list_attachments(){
 var oReq = new XMLHttpRequest();
    oReq.onload = function() {
        var attachments = JSON.parse(this.responseText);
        categories_list = document.getElementById("n_attachments_body")
        while (categories_list.firstChild) {categories_list .removeChild(categories_list.firstChild);}
        for (let a in attachments) {
			var a_filepath = attachments[a]['filepath'];
            var node = document.createElement("div");
            var filenamenode = document.createElement("H1");
			filenamenode.innerHTML = attachments[a]['filename'];
            filenamenode.title = attachments[a]['filename'];
            node.setAttribute("onclick",`n_open_attachment("` + (a_filepath) + `")`);
            node.appendChild(filenamenode);			 
            var fileinfonode = document.createElement("H2");
            var onclickstring = "n_delete_attachment(\"" + a_filepath + "\")";
            
            var deletenode = document.createElement("a");
            deletenode.innerHTML = "delete";
            
			fileinfonode.innerHTML = attachments[a]['filesize'] + ", " + attachments[a]['filetime'] + " - ";
            deletenode.setAttribute("onclick", onclickstring);
            
            fileinfonode.appendChild(deletenode);

            node.appendChild(fileinfonode); 
            var filetypenode = document.createElement("span");
			node.appendChild(filetypenode);
			var filetypetxt = (attachments[a]['filename'].split('.')[1]).toLowerCase();
			if(filetypetxt === 'png' || filetypetxt === 'jpg' || filetypetxt === 'tga' || filetypetxt === 'gif')
			{	
				filetypenode.style = `background : url('/functions/load_img.php?img=` + encodeURIComponent(a_filepath) + `'); background-size:cover; background-position:center; background-repeat: no-repeat;`;
			}
			else
			{
				filetypenode.innerHTML = "." + attachments[a]['filename'].split('.')[1];
			}
           
            categories_list.appendChild(node); 
        }
		n_attachment_count.innerHTML= " (" + attachments.length + ")";
		document.getElementById("n_attachments_header").childNodes[0].textContent = "Attachments for " + v_note_path;
    };
    oReq.open("get", "functions/list_attachments.php" + "?note=" + encodeURIComponent(v_note_path) + "&trick=" + Date.now(), true);
    oReq.send();
}

function n_open_attachment(attachment)
{
        
        window.open("functions/load_attachment.php?note=" + encodeURIComponent(v_note_path) + "&attachment=" + encodeURIComponent(attachment));
}

function n_delete_attachment(attachmentname){
    event.stopPropagation();
	if(confirm(`you're about to permanently delete the following attachment:\n\"${attachmentname}\"\nAre you sure?\npress OK to remove\n\n`))
	{
	 	var oReq = new XMLHttpRequest();
	    oReq.onload = function() {
    	    var attachments = JSON.parse(this.responseText);
			popToast("Attachment deleted");
    	};
	    oReq.open("get", "functions/delete_attachment.php" + "?attachment=" + encodeURIComponent(attachmentname) + "&trick=" + Date.now(), true);
    	oReq.send();
		list_attachments();
	}
	else
	{	

	}
}

function n_upload_attachment(){
    
    if(document.getElementById("fileinput").files.length === 0)
    {
        popToast("no files selected", false);
    }
    else
    {
        var fd = new FormData();
        fd.append("image", document.getElementById("fileinput").files[0]);
        var xmlHTTP = new XMLHttpRequest();
        xmlHTTP.open("POST", "functions/upload_image.php", true);
        xmlHTTP.onload = function() {
            var response = JSON.parse(this.responseText);
            if(response.hasOwnProperty('error'))
            {
                popToast(response.error, false);
            }
            else
            {
                popToast("success", true);
            }
			popToast(this.responseText);
            document.getElementById("fileinput").value = '';
            list_attachments();
            
    	};
        xmlHTTP.send(fd);
    }
}

function n_attachments_show(){
	    document.getElementById("n_attachments_container").style.display = "block";
	    document.getElementById("n_attachments_blackout").style.display = "block";
        document.getElementById("fileinput").value = '';

		list_attachments();
}

function n_attachments_hide(){
	    document.getElementById("n_attachments_blackout").style.display = "none";
	    document.getElementById("n_attachments_container").style.display = "none";
}

function select_note(note){
    if(!check_for_edits())
    {
        popToast("There are unsaved changes to the current note. Please <b>save</b> or <b>discard</b> these first.", false);
        return;
    }
    
    if(note != null)
    {
        v_note_path = v_category + "/" + note;
        load_note();
    }
}


function update_breadcrumb()
{
    brdcrmb = "please select a note...";
    if(!(v_note_path === null))
    {
        brdcrmb = v_note_path;
    }
    document.getElementById("n_breadcrumb").innerHTML = brdcrmb;
    document.title = 'kokosnote - ' + brdcrmb;
}

function create_note()
{
    if(v_category === null){popToast("please select a category first", false);}
    else
    {
        var p = prompt('note name:');
        if (p != null && p && p.length > 0)
        {
            p = sanitize(p, '');
            if(v_notes.includes(p))
            {
                popToast('this note already exists', false);
            }
            else
            {
                var xhr = new XMLHttpRequest();
                xhr.open('PUT', 'functions/save_note.php?note=' + encodeURIComponent(v_category + "/" + p) + "&trick=" + Date.now());
                xhr.setRequestHeader('Content-Type', 'application/json');
                xhr.onload = function() {
                    if (xhr.status === 200) {
                        popToast("Note created.", true);
                        refresh_notes();
                        select_note(p);
                    }
                };
                xhr.send('');
            }
        }
    }
}

function delete_note()
{
    if(v_note_path === null)
    {
        popToast('no note selected - nothing to delete!', false);
    }
    else
    {
        if(confirm(`you're about to permanently remove the following note:\n\"${v_note_path}\"\npress OK to remove\n\n`))
        {
            var xhr = new XMLHttpRequest();
            xhr.open('PUT', 'functions/delete_note.php?note=' + encodeURIComponent(v_note_path) + "&trick=" + Date.now());
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    popToast("note removed.", true);
                    refresh_notes();
                    document.getElementById('n_body').classList.add('n_inactive');
                    easyMDE.value('');
                    previous_content = '';
                    v_note_path = null;
                    update_breadcrumb();
                }
            };
            xhr.send('');   
        }
    }
}


function create_category()
{
    var p = prompt('category name:');
    if (p != null && p && p.length > 0)
    {
        p = sanitize(p, '');
        if(v_categories.includes(p))
        {
            alert('this category already exists');
        }
        else
        {
            var xhr = new XMLHttpRequest();
            xhr.open('PUT', 'functions/make_category.php?name=' + encodeURIComponent(p) + "&trick=" + Date.now());
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    popToast("category created", true);
                    refresh_categories();
                    select_category(p);
                }
            };
            xhr.send('');
        }
    }
}

function rename_note()
{
    var p = prompt('rename\n' + v_note_path.split('/')[1] + '\nto:');
    if (p != null && p && p.length > 0)
    {
        p = sanitize(p, '');
        
        {
            var xhr = new XMLHttpRequest();
            xhr.open('PUT', 'functions/rename_note.php?old_note=' + encodeURIComponent(v_note_path.split('/')[1]) + "&cat=" + encodeURIComponent(v_note_path.split('/')[0]) + "&new_note="+ encodeURIComponent(p) + "&trick=" + Date.now());
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    response =  JSON.parse(xhr.responseText);
                    if(response.hasOwnProperty('error'))
                    {
                        popToast(response.error, false);
                    }
                    else
                    {
                        popToast(response.success, true);
                        refresh_notes();
                        document.getElementById('n_body').classList.add('n_inactive');
                        easyMDE.value('');
                        previous_content = '';
                        v_note_path = null;
                        update_breadcrumb();
                        refresh_categories();
                    }
                }
            };
            xhr.send('');
        }
    }    
}

function rename_cat()
{
    var p = prompt('rename\n' + v_category + '\nto:');
    if (p != null && p && p.length > 0)
    {
        p = sanitize(p, '');
        
        {
            var xhr = new XMLHttpRequest();
            xhr.open('PUT', 'functions/rename_cat.php?old_cat=' + encodeURIComponent(v_category) + "&new_cat=" + encodeURIComponent(p) + "&trick=" + Date.now());
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    response =  JSON.parse(xhr.responseText);
                    if(response.hasOwnProperty('error'))
                    {
                        popToast(response.error, false);
                    }
                    else
                    {
                        popToast(response.success, true);
                        refresh_notes();
                        document.getElementById('n_body').classList.add('n_inactive');
                        easyMDE.value('');
                        previous_content = '';
                        v_note_path = null;
                        update_breadcrumb();
                        refresh_categories();
                    }
                }
            };
            xhr.send('');
        }
    }    
}


function remove_cat()
{
    if(confirm(`you're about to permanently remove the following category:\n\"${v_category}\"\nAre you ABSOLUTELY sure?\npress OK to remove\n\n`))
    {       
        {
            var xhr = new XMLHttpRequest();
            xhr.open('PUT', 'functions/delete_cat.php?cat=' + encodeURIComponent(v_category) + "&trick=" + Date.now());
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    response =  JSON.parse(xhr.responseText);
                    if(response.hasOwnProperty('error'))
                    {
                        popToast(response.error, false);
                    }
                    else
                    {
                        popToast(response.success, true);
                        refresh_notes();
                        document.getElementById('n_body').classList.add('n_inactive');
                        easyMDE.value('');
                        previous_content = '';
                        v_note_path = null;
                        update_breadcrumb();
                        refresh_categories();
                    }
                }
            };
            xhr.send('');
        }
    }    
}

function update_selected_highlight()
{
    categories_list = document.getElementById("n_categories");
    var children = categories_list.children;
    for (var i = 0; i < children.length; i++) {
        var child = children[i];
        if(child.innerHTML === v_category)
            child.classList.add('n_item_selected');
        else
            child.classList.remove('n_item_selected');
    }
    
    if(!v_note_path)
    {
        return;
    }
    
    notes_list = document.getElementById("n_notes");
    children = notes_list.children;
    l_note = v_note_path.split('/')[1];
    if (v_category === v_note_path.split('/')[0])
    {
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if(child.innerHTML === l_note)
                child.classList.add('n_item_selected');
            else
                child.classList.remove('n_item_selected');
        }
    }
}

function n_discard()
{
    if(!check_for_edits())
    {
        if(confirm(`there are unsaved changes for\n\"${v_note_path}\"\npress OK to discard\n\n`))
        {
            popToast("Changes discarded.", false);
            easyMDE.value(previous_content);
        }
        else
        {
            popToast("Did not discard.", true);
            return;
        }
    }
    else
    {
        popToast("Nothing to discard.", true);
    }
}

function toggleSidebar()
{
        var element = document.getElementById("n_sidebar");
        element.classList.toggle("foldaway");
        if(!element.classList.contains("foldaway"))
        {        document.getElementById('n_body').classList.add('n_inactive'); }
        else
        {        document.getElementById('n_body').classList.remove('n_inactive'); }
}


var previous_content = '';

function check_for_edits()
{
    if (previous_content === easyMDE.value())
    {
        return true;
    }
    else
    {
        return false; //there are changes
    }
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function popToast(message, good){
    
            console.log(message);
            toaster = document.getElementById("toaster");
            var node = document.createElement("div");
            if(good)
                node .classList.add("toast_good");
            else
                node .classList.add("toast_bad");
            node.innerHTML = message;
            toaster.appendChild(node); 
            
            setTimeout(function(){
                node .classList.add("toast_hidden");
            }, 1700);
            
            setTimeout(function(){
                node.remove();
            }, 2000);
}
