# 🥥 kokosnote

`kokosnote` is a _simple, under-engineerd_ note taking app. It can be deployed anywhere that runs PHP.

`kokosnote` is for you if:
- you want to securely share notes
- you have a webserver somewhere with Apache (others might work too)
- you dislike hassle

----
![](https://gitlab.com/thomaskole/kokosnote/-/wikis/uploads/b40bf5717f7ada6689dd643842d8ea09/kokos_a.png)

![](https://gitlab.com/thomaskole/kokosnote/-/wikis/uploads/cc6bed7a2cbcadd64010e53a1cd60139/kokos_b.png)

----

### 🥥 features:
- barebones
- notes are stored in plain text on your server.
- uses [easy markdown editor (github.com)](https://github.com/Ionaru/easy-markdown-editor).
- besides this, no other dependencies.
- image upload
- attachment upload
- password protection
- drag-and-drop ftp deploy

----

### 🥥 installation:

1. download this repository as a .zip
2. extract the contents to your server, preferrably in a subdomain (kokos.example.com, for example)
3. (now you should have kokos.example.com/index.php, etc.)
4. change your password in config.php
5. done

p.s.:\
for Linux users: to test the application locally, simply `cd` to the root folder and run `php -S localhost:8000`\
then navigate to `localhost` in your favorite browser. The default password is `Example`

----

### 🥥 advanced:

Your notes are stored in /data/. Each category gets its own folder, each note its own .md file.\
so, your note about kimchi in recipies is found in /data/recipies/kimchi.md.\
All related attachments are stored in a folder with the same name as the note.

If you ever feel the need to back up your notes locally, just get the /data/ folder via ftp.\
alternatively, if you're that type, you can call `kokos.example.com/functions/takeout.php?supersecretpassword=pwd` to download all your notes as a zip.

/data/ is theorhetically served to the public, however, an .htaccess should prevent this. Please try this out by visiting `kokos.example.com/data/` to see if you get a `403 forbidden`.

In order to avoid conflicts when 2 people are working on the same note, filemtime is taking into account. If you overwrite the changes made by the other person, the file is backed up as `kimchi_xxxxxxxxxx.md` and yours is saved.

Images are uploaded to `/data/category/note/your_image.png`. Images don't get deleted when you stop referencing them in your note! The only way to really remove them is by deleting the note.

----
### 🥥 why?:

I tried various self-hosted note taking solutions, but all of them were over-engineerd, bloated, or a bit too simple.\
Note taking should not require a _whole operating system_.\
So, here you go. Let me know if there's anything wrong with it.

----
### 🥥 Architecture:

There's an index.php, which requires the user to be logged in. Otherwise it redirects to login.php\
Once logged in, the user has access to a UI which makes various HttpRequests via functions.js to .php files found in /functions.\
Each function has a file, for example returning a list of all notes, or saving a note.\
One big .css file handles the style - including overrides for the EasyMDE rules. I've decided to override rules to keep the original EasyMDE css intact in case of future upgrades.
