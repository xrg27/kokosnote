<?php
    require "rename_function.php";

    require_once('../protect.php');
        
    if(file_exists('../data/'.$_GET['new_cat']))
    {
        $json_r['error'] = "looks like that category already exists!";
    }
    else
    {
        if( mkdir ( '../data/'.$_GET['new_cat'], 0777 , true))
        {
            $notes = glob('../data/'.$_GET['old_cat'].'/*.md');
        
            foreach ($notes as &$t_note) {
                $t_note = pathinfo($t_note)['filename'];
                $json_r = renameNote($_GET['old_cat'], $_GET['new_cat'], $t_note, $t_note);
                if(array_key_exists('error', $json_r))
                {
                    echo json_encode($json_r);
                    exit;
                }
            }
            
            rmdir('../data/'.$_GET['old_cat']);
            $json_r['success'] = "Renamed\n\"".$_GET['old_cat']."\"\nto\n\"".$_GET['new_cat']."\"\nand moved all its files.";
        }
        else
        {
            $json_r['error'] = "Something went wrong creating the new directory";
        }
    }
       
    ob_clean();
    echo json_encode($json_r);
?>
